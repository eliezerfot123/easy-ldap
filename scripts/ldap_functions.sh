#!/bin/bash

# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.2-alpha
# Copyright (C) 2010 Jesus Lara

# ldap_functions
# incorpora todas las funciones de configuracion y pruebas de servidor openldap

## ldap commands #######

SLAPADD="$(which slapadd)"
SLAPPASSWD="$(which slappasswd)"
SLAPINDEX="$(which slapindex) -F $LDAP_DIRECTORY -n 2"
SLAPTEST="$(which slaptest) -d2 -u"
SLAPACL="$(which slapacl) -F $LDAP_DIRECTORY -v"

LDAPADD="$(which ldapadd) -H ldapi:/// -Y EXTERNAL -Q"
LDAPADDUSER="$(which ldapadd) -H ldapi:/// -x "
LDAPSEARCH="$(which ldapsearch) -H ldapi:///"

#############################################################

# configure basic ldap (cn=admin, cn=config, schemas, database)
function ldap_configure() {
echo
echo " == Configuracion de openLDAP == "
echo

# configurando /etc/default/slapd
sed -i "s/SLAPD_SERVICES=\"ldap:\/\/\/ ldapi:\/\/\/\"/SLAPD_SERVICES=\"ldap:\/\/\/ ldapi:\/\/\/ ldaps:\/\/\/\"/g" /etc/default/$LDAP_SERVER

# clave tanto de usuario cn=admin,cn=config como del usuario admin del LDAP
get_admin_password

echo "Clave cifrada: "
echo `$SLAPPASSWD -uvs $PASS`
echo

## pruebas finales
# determinamos que la configuracion es correcta
$SLAPTEST -F $LDAP_DIRECTORY
if [ "$?" -ne "0" ]; then
	echo "Configuracion incorrecta, revisar configuracion de openldap"
	exit 1
fi

# configuracion basica de cn=config
$LDAPADD << EOF
dn: cn=config
changetype: modify
replace: olcAttributeOptions
olcAttributeOptions: lang-
-
replace: olcToolThreads
olcToolThreads: 8
-
replace: olcThreads
olcThreads: 32
-
replace: olcSockbufMaxIncoming
olcSockbufMaxIncoming: 262143
-
replace: olcSockbufMaxIncomingAuth
olcSockbufMaxIncomingAuth: 16777215
-
replace: olcReadOnly
olcReadOnly: FALSE
-
replace: olcReverseLookup
olcReverseLookup: FALSE
-
replace: olcServerID
olcServerID: 1 ldap://$SERVERNAME
EOF

# configuracion de olcDatabase(0)
$LDAPADD << EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: `$SLAPPASSWD -uvs $PASS`
-
replace: olcAccess
olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by * break
olcAccess: {1}to dn="" by * read
olcAccess: {2}to dn.subtree="" by * read
olcAccess: {3}to dn="cn=Subschema" by * read
EOF

# habilitando todos los modulos necesarios:
$LDAPADD << EOF
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleload: back_bdb
olcModuleload: unique
olcModuleload: back_dnssrv
olcModuleload: back_ldap
olcModuleload: syncprov
olcModuleload: dynlist
olcModuleload: refint
olcModuleload: constraint
olcModuleload: back_monitor
olcModuleload: back_perl
olcModuleload: back_shell
olcModuleload: ppolicy
olcModuleload: accesslog
olcModuleload: auditlog
olcModuleload: valsort
olcModuleload: memberof
EOF

# tunning de la DB
$LDAPADD << EOF
dn: olcDatabase={1}hdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: `$SLAPPASSWD -uvs $PASS`
-
replace: olcLastMod
olcLastMod: TRUE
-
replace: olcAddContentAcl
olcAddContentAcl: TRUE
-
replace: olcSizeLimit
olcSizeLimit: 2000
-
replace: olcTimeLimit
olcTimeLimit: 60
-
replace: olcDbIDLcacheSize
olcDbIDLcacheSize: 500000
-
replace: olcDbCacheFree
olcDbCacheFree: 1000
-
replace: olcDbDNcacheSize
olcDbDNcacheSize: 0
-
replace: olcDbCacheSize
olcDbCacheSize: 5000
-
replace: olcDbCheckpoint
olcDbCheckpoint: 1024 30
-
replace: olcDbConfig
olcDbConfig: {0}set_cachesize 0 10485760 0
olcDbConfig: {1}set_lk_max_objects 1500
olcDbConfig: {2}set_lk_max_locks 1500
olcDbConfig: {3}set_lk_max_lockers 1500
olcDbConfig: {4}set_lg_bsize 2097152
olcDbConfig: {5}set_flags DB_LOG_AUTOREMOVE
EOF
}

function schema_configure() {
echo
echo " == Configuracion de Schemas == "
echo
# detener el servidor LDAP
$LDAP_SERVICE stop
# eliminamos los schemas antiguos
rm $LDAP_DIRECTORY/cn\=config/cn\=schema/*
# copiamos los nuevos esquemas
cp $DIRECTORY/data/schemas/* $LDAP_DIRECTORY/cn\=config/cn\=schema/
# asignar el propietario y grupo:
chown $LDAP_USER:$LDAP_GROUP $LDAP_DIRECTORY -R
# pruebo la configuracion
$SLAPTEST -F $LDAP_DIRECTORY
if [ "$?" -ne "0" ]; then
	echo "Incorrecta configuracion de schemas, revise los logs"
	exit 1
fi
# si todo esta bien, iniciamos
$LDAP_SERVICE start
sleep 1
# e indexamos
$LDAPADD << EOF
dn: olcDatabase={1}hdb,cn=config
changetype: modify
replace: olcDbIndex
olcDbIndex: objectClass eq
olcDbIndex: uid eq,approx,sub
olcDbIndex: telephoneNumber eq,sub,pres
olcDbIndex: accountStatus eq
olcDbIndex: uidNumber,gidNumber,memberUid,loginShell eq
#indice para entradas cn,ou,o,sn,uid
olcDbIndex: cn,sn,ou,o eq,pres,sub,subinitial
#indice para busqueda de nombres,apellidos, etc
olcDbIndex: givenname,displayName eq,subinitial,approx
olcDbIndex: employeeType,employeeNumber,l pres,eq
#indice para cuentas de correo
olcDbIndex: mail pres,eq,sub
olcDbIndex: mailAlternateAddress,mailForwardingAddress eq,sub
olcDbIndex: associatedDomain,uniqueMember,mailQuotaSize,mailQuotaCount,mailSizeMax eq
# posixAccount
olcDbIndex: homeDirectory,gecos eq
#indices para Samba
olcDbIndex: sambaSIDList,dc eq,pres
olcDbIndex: sambaGroupType eq,pres
olcDbIndex: sambaSID eq,pres,sub
olcDbIndex: sambaDomainName,sambaPrimaryGroupSID eq
#indice para NIS
olcDbIndex: nisMapName,nisMapEntry eq,pres,sub
# indices para asterisk
olcDbIndex: AstAccountHost eq
olcDbIndex: AstAccountIPAddress,AstAccountPort,AstContext,AstPriority eq
olcDbIndex: AstExtension eq,pres,sub
# indices para el dhcp
olcDbIndex: dhcpHWAddress   eq
olcDbIndex: dhcpClassData   eq
olcDbIndex: dhcpPrimaryDN   eq
olcDbIndex: dhcpSecondaryDN eq
# indices para kerberos
olcDbIndex: krbPrincipalName eq
olcDbIndex: krbPwdPolicyReference eq
# indices para el DNS
olcDbIndex: relativeDomainName,dNSTTL,dNSClass,zoneName,sOARecord eq
olcDbIndex: aRecord,mXRecord,nSRecord eq
# indice para el sudo
olcDbIndex: sudoUser,sudoHost eq,sub
# indice para replicacion
olcDbIndex: entryCSN,entryUUID pres,eq
EOF
}

function sasl_configure() {
echo
echo " == Seguridad SASL == "
echo
# instalar SASL
$INSTALLER install $SASL_PKGS
# esperamos
sleep 1
# configuramos e iniciamos saslauthd
$SED -i 's/START=no/START=yes/g' /etc/default/saslauthd
$SED -i "s/MECHANISMS=.*$/MECHANISMS=\"ldap pam kerberos5\"/g" /etc/default/saslauthd

# configuramos saslauthd
cat <<EOF > /etc/saslauthd.conf
ldap_servers: ldap://$SERVERNAME/
ldap_auth_method: bind
ldap_bind_dn: cn=admin,$LDAP_SUFFIX
ldap_bind_pw: $PASS
ldap_version: 3
ldap_search_base: $LDAP_SUFFIX
ldap_filter: (uid=%U)
ldap_verbose: on
ldap_scope: sub
 #SASL info
ldap_default_realm: $DOMAIN
ldap_use_sasl: no
ldap_debug: 3
EOF

# reiniciamos el servicio
/etc/init.d/saslauthd restart
# configuramos SASL en openLDAP:

$LDAPADD << EOF
dn: cn=config
changetype:modify
replace: olcPasswordHash
olcPasswordHash: {SSHA}
-
replace: olcSaslSecProps
olcSaslSecProps: noplain,noanonymous,minssf=56
-
replace: olcAuthzPolicy
olcAuthzPolicy: none
-
replace: olcConnMaxPendingAuth
olcConnMaxPendingAuth: 1000
-
replace: olcSaslHost
olcSaslHost: $SERVERNAME
-
replace: olcSaslRealm
olcSaslRealm: $DOMAIN
EOF

# configuramos SASL en la DB
$LDAPADD << EOF
dn: cn=config
changetype: modify
replace: olcAuthzRegexp
olcAuthzRegexp: uid=(.*),cn=.*,cn=.*,cn=auth ldap:///??sub?(uid=$1)
EOF

# verificamos el acceso a los mecanismos SASL
echo
echo " Verificando acceso a todos los mecanismos SASL "
echo
$LDAPSEARCH -x -b '' -s base -LLL supportedSASLMechanisms
if [ "$?" -ne "0" ]; then
   echo "Error: acceso a los mecanismos SASL, alto"
   exit 1
fi
}

function ssl_configure() {
# creamos el directorio SSL:
mkdir /etc/ldap/ssl -p

# copiamos los certificados Debian basicos:
if [ -f "/etc/ssl/certs/ssl-cert-snakeoil.pem" ]; then
cp /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/ldap/ssl
fi

if [ -f "/etc/ssl/private/ssl-cert-snakeoil.key" ]; then
cp /etc/ssl/private/ssl-cert-snakeoil.key /etc/ldap/ssl
fi

if [ -f "/etc/ssl/certs/ca.pem" ]; then
cp /etc/ssl/certs/ca.pem /etc/ldap/ssl
fi

$LDAPADD << EOF
dn: cn=config
changetype:modify
replace: olcLocalSSF
olcLocalSSF: 71
-
replace: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ldap/ssl/ca.pem
-
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ldap/ssl/ssl-cert-snakeoil.pem
-
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ldap/ssl/ssl-cert-snakeoil.key
-
replace: olcTLSVerifyClient
olcTLSVerifyClient: never
-
replace: olcTLSCipherSuite
olcTLSCipherSuite: +RSA:+AES-256-CBC:+SHA1
EOF
}

function log_configure() {
echo
echo " == Configuracion de Logging == "
echo
# crear carpeta para logging
if [ ! -d "/var/log/slapd" ]; then
	mkdir /var/log/slapd
fi
chmod 755 /var/log/slapd/
chown $LDAP_USER:$LDAP_GROUP /var/log/slapd/ -R
# cambiamos rsyslog
# Redirect all log files through rsyslog.
sed -i "/local4.*/d" /etc/rsyslog.conf

# si no se encuentra la linea, se agrega a rsyslog
if [ `cat /etc/rsyslog.conf | grep slapd.log | wc -l` == "0" ]; then
cat >> /etc/rsyslog.conf << EOF
local4.*                        /var/log/slapd/slapd.log
EOF
fi

# configurando el log del LDAP
$LDAPADD << EOF
dn: cn=config
changetype:modify
replace: olcLogFile
olcLogFile: /var/log/slapd/slapd.log
EOF

# configurando nivel de logging
$LDAPADD << EOF
dn: cn=config
changetype:modify
replace: olcLogLevel
olcLogLevel: config stats shell
-
replace: olcIdleTimeout
olcIdleTimeout: 30
-
replace: olcGentleHUP
olcGentleHUP: FALSE
-
replace: olcConnMaxPending
olcConnMaxPending: 100
EOF

# configurando logrotate
cat <<EOF > /etc/logrotate.d/slapd
/var/log/slapd/slapd.log {
        daily
        missingok
        rotate 7
        compress
        copytruncate
        notifempty
        create 640 openldap openldap
}
EOF
# reiniciando rsyslog
/etc/init.d/rsyslog restart
}

# configuracion de reglas de control de acceso
function acl_configure() {
echo
echo " == Configuracion de reglas ACL == "
echo
# configuracion de las reglas de control de acceso
$LDAPADD << EOF
dn: olcDatabase={1}hdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to attrs=userPassword,sambaNTPassword,sambaLMPassword,sambaPwdLastSet,sambaPwdMustChange,sambaPasswordHistory,shadowLastChange,shadowMin,shadowMax,shadowWarning,shadowInactive,shadowExpire,shadowFlag,pwdChangedTime,pwdAccountLockedTime,pwdFailureTime,pwdHistory,pwdGraceUseTime,pwdReset by self write by anonymous auth by dn="cn=admin,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" manage by group/groupOfNames/member.exact="cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to attrs=carLicense,homePhone,mobile,pager,telephoneNumber by self write by set="this/manager & user" write by set="this/manager/secretary & user" write by dn="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write
## sudoers
olcAccess: {3}to dn.subtree="cn=sudoers,cn=servicios,$LDAP_SUFFIX" by * read
## proteccion de atributos especiales
olcAccess: {4}to attrs=gidNumber,uidNumber,homeDirectory,uid,loginShell,gecos by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" manage by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" manage by dn="cn=admin,$LDAP_SUFFIX" manage by group.exact="cn=lectores,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by dn="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by group.exact="cn=replicatos,cn=grupos,cn=servicios,$LDAP_SUFFIX" write
# y de las politicas
olcAccess: {5}to dn.subtree="cn=politicas,cn=servicios,$LDAP_SUFFIX" by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" manage by group/groupOfNames/member.exact="cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by * read
olcAccess: {6}to dn.subtree="$LDAP_SUFFIX" attrs=sambaLMPassword,sambaNTPassword by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" manage by anonymous auth by self write by * none
olcAccess: {7}to dn.subtree="$LDAP_SUFFIX" attrs=sambaPasswordHistory,pwdHistory by self read by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=sistema,$LDAP_SUFFIX" write by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" manage by * none
# regla final
olcAccess: {8}to dn.subtree="$LDAP_SUFFIX" by self write by dn="cn=admin,$LDAP_SUFFIX" write by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" write by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" manage by group/groupOfNames/member.exact="cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by * read
olcAccess: {8}to * by self write by dn="cn=admin,$LDAP_SUFFIX" write by set="[cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX]/memberUid & user/uid" write by dn="cn=administrador,cn=usuarios,cn=servicios,$LDAP_SUFFIX" write  by  by group/groupOfNames/member.exact="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" manage by group/groupOfNames/member.exact="cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by * read
EOF
echo
echo " == Configuracion de Limites == "
echo
# configuracion de los limites:
$LDAPADD << EOF
dn: olcDatabase={1}hdb,cn=config
changetype: modify
add: olcLimits
olcLimits: {0}dn.base="cn=admin,$LDAP_SUFFIX" size.soft=unlimited  size.hard=unlimited  time.soft=unlimited  time.hard=unlimited
olcLimits: {1}group/groupOfNames/member="cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX" size=unlimited time=unlimited
olcLimits: {2}group/groupOfNames/member="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" size=unlimited time=unlimited
olcLimits: {3}group/groupOfNames/member="cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" size=unlimited time=unlimited
EOF
# prueba de autenticacion
# reinicio el servicio
$LDAP_SERVICE restart
sleep 1
echo
echo " == Prueba de ACLs == "
echo " * - Verificar que las reglas permiten el acceso a la OU gente"
echo
$SLAPACL -D "cn=administrador,cn=usuarios,cn=servicios,$LDAP_SUFFIX" -b "$LDAP_SUFFIX" "ou/write:gente"
}

function dit_configure() {
echo
echo " == DIT Basico == "
echo
$LDAPADDUSER -D "cn=admin,$LDAP_SUFFIX" -w $PASS << EOF
dn: cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: servicios

dn: cn=politicas,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: politicas

dn: cn=sudoers,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: sudoers

dn: cn=defaults,cn=sudoers,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: sudoRole
cn: defaults
description: Opciones por defecto para sudo
sudoOption: ignore_dot
sudoOption: !mail_no_user
sudoOption: log_host
sudoOption: logfile=/var/log/sudo.log
sudoOption: requiretty
sudoOption: always_set_home
sudoOption: env_reset
sudoOption: env_keep="COLORS DISPLAY HOSTNAME HISTSIZE INPUTRC KDEDIR LS_COLORS"
sudoOption: env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
sudoOption: env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
sudoOption: env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
sudoOption: env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"
sudoOption: secure_path=/sbin:/bin:/usr/sbin:/usr/bin
sudoOption: !syslog
sudoOption: timestamp_timeout=10

dn: cn=root,cn=sudoers,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: sudoRole
cn: root
sudoUser: root
sudoHost: ALL
sudoRunAsUser: ALL
sudoCommand: ALL
description: sudo-role for root user

dn: cn=%administradores,cn=sudoers,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: sudoRole
cn: %administradores
description: Permitir acceso sudo a todos los administradores
sudoCommand: ALL
sudoHost: ALL
sudoRunAsUser: ALL
sudoUser: %administradores

dn: cn=dhcp,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: dhcp

dn: cn=samba,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: samba

dn: cn=usuarios,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: usuarios

dn: cn=ldap-monitor,cn=usuarios,cn=servicios,$LDAP_SUFFIX
objectClass: simpleSecurityObject
objectClass: organizationalRole
objectClass: top
cn: ldap-monitor
userPassword: MTIzNDU2
description: Usuario del monitoreo del LDAP

dn: cn=administrador,cn=usuarios,cn=servicios,$LDAP_SUFFIX
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: top
cn: administrador
gidNumber: 999
uidNumber: 999
homeDirectory: /etc/skel
sn: administrador
uid: administrador
givenName: Administrador
gecos: Administrador $DOMAIN
loginShell: /bin/bash
preferredLanguage: es

dn: cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: grupos

dn: cn=administradores,cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: posixGroup
objectClass: namedObject
cn: administradores
gidNumber: 999
memberUid: administrador
description: Grupo de Administradores Posix de $DOMAIN

dn: cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: groupOfNames
cn: ldap admins
member: cn=admin,$LDAP_SUFFIX
member: cn=administrador,cn=usuarios,cn=servicios,$LDAP_SUFFIX
description: Grupo de administradores del LDAP

dn: cn=account admins,cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: groupOfNames
cn: account admins
member: cn=admin,$LDAP_SUFFIX
member: cn=administrador,cn=usuarios,cn=servicios,$LDAP_SUFFIX
description: Grupo de administradores del LDAP

dn: cn=replicators,cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: groupOfNames
cn: replicators
member: cn=ldap-monitor,cn=usuarios,cn=servicios,$LDAP_SUFFIX
description: Grupo con permiso de lectura especial sobre el LDAP

dn: cn=ldap monitors,cn=grupos,cn=servicios,$LDAP_SUFFIX
objectClass: top
objectClass: groupOfNames
cn: ldap monitors
member: cn=ldap-monitor,cn=usuarios,cn=servicios,$LDAP_SUFFIX
description: Grupo con permiso de lectura especial monitoreo

dn: ou=hosts,$LDAP_SUFFIX
objectClass: top
objectClass: organizationalUnit
ou: hosts
description: hosts de $DOMAIN

dn: cn=$HOSTNAME_PREFIX,ou=hosts,$LDAP_SUFFIX
objectClass: ipHost
objectClass: device
objectClass: top
cn: $HOSTNAME_PREFIX
cn: $SERVERNAME
ipHostNumber: $LAN_IPADDR
manager: cn=admin,$LDAP_SUFFIX
o: $DOMAIN

dn: ou=grupos,$LDAP_SUFFIX
objectClass: top
objectClass: organizationalUnit
ou: grupos
description: grupos de $DOMAIN

dn: ou=gente,$LDAP_SUFFIX
objectClass: top
objectClass: organizationalUnit
ou: gente
description: usuarios de $DOMAIN

dn: ou=equipos,$LDAP_SUFFIX
objectClass: top
objectClass: organizationalUnit
ou: equipos
description: computadoras y equipos de $DOMAIN

dn: cn=organizacion,$LDAP_SUFFIX
objectClass: top
objectClass: namedObject
cn: organizacion
EOF
}

# configure database monitor
function cnmonitor_configure(){
echo
echo " == cn=Monitor  == "
echo
$LDAPADD << EOF
dn: olcDatabase={3}monitor,cn=config
objectClass: olcDatabaseConfig
olcDatabase: {3}monitor
olcAccess: {0}to * by dn.exact="cn=admin,$LDAP_SUFFIX" write by * none
olcAccess: {1}to dn.subtree="cn=monitor" by dn.exact="cn=admin,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by group/groupOfNames/member.exact="cn=ldap monitors,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by users read by * none
olcAccess: {2}to dn.children="cn=monitor" by dn.exact="cn=admin,$LDAP_SUFFIX" write by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by group/groupOfNames/member.exact="cn=ldap monitors,cn=grupos,cn=servicios,$LDAP_SUFFIX" read
olcLastMod: TRUE
olcMaxDerefDepth: 15
olcReadOnly: FALSE
olcRootDN: cn=config
olcMonitoring: TRUE
EOF
# y agregamos reglas de control de acceso en frontend
$LDAPADD << EOF
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by * break
olcAccess: {1}to dn.exact="" by * read
olcAccess: {2}to dn.base="cn=Subschema" by * read
olcAccess: {3}to dn.subtree="cn=monitor" by dn="cn=admin,$LDAP_SUFFIX" read
olcAccess: {4}to dn.subtree="" by group/groupOfNames/member.exact="cn=ldap admins,cn=grupos,cn=servicios,$LDAP_SUFFIX" read by group/groupOfNames/member.exact="cn=ldap monitors,cn=grupos,cn=servicios,$LDAP_SUFFIX" read
EOF
}

# openLDAP accesslog
function accesslog_configure(){
echo
echo " == openLDAP Accesslog == "
echo
# creamos el directorio de la DB accesslog
mkdir /var/lib/ldap/accesslog
# se copia DB_CONFIG al directorio
cp -p /var/lib/ldap/DB_CONFIG /var/lib/ldap/accesslog
chown $LDAP_USER:$LDAP_GROUP /var/lib/ldap/accesslog -R
# cargamos la DB de accesslog
$LDAPADD << EOF
dn: olcDatabase={2}hdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcHdbConfig
olcDatabase: {2}hdb
olcDbDirectory: /var/lib/ldap/accesslog
olcSuffix: cn=accesslog
olcRootDN: cn=admin,$LDAP_SUFFIX
olcDbIndex: default eq
olcDbIndex: entryCSN,objectClass,reqEnd,reqResult,reqStart
EOF

# se carga el overlay, asociado a la primera DB
$LDAPADD << EOF
dn: olcOverlay=accesslog,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcAccessLogConfig
olcOverlay: accesslog
olcAccessLogDB: cn=accesslog
olcAccessLogOps: writes
olcAccessLogSuccess: TRUE
# scan the accesslog DB every day, and purge entries older than 7 days
olcAccessLogPurge: 07+00:00  01+00:00
EOF

}

# db de auditoria
function auditlog_configure(){
echo
echo " == Audit Log == "
echo
# creamos el archivo
touch /var/log/slapd/audit.ldif
chown $LDAP_USER:$LDAP_GROUP /var/log/slapd/audit.ldif
# y cargamos la regla de auditoria
$LDAPADD << EOF
dn: olcOverlay=auditlog,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcAuditLogConfig
olcOverlay: auditlog
olcAuditlogFile: /var/log/slapd/audit.ldif
EOF
}

# configure overlays basicos
function overlay_configure(){
echo
echo " == Carga de modulos (overlays) == "
echo
echo " = referencial integrity = "
$LDAPADD << EOF
dn: olcOverlay=refint,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcRefintConfig
objectClass: olcOverlayConfig
objectClass: olcConfig
objectClass: top
olcOverlay: refint
olcRefintAttribute: member
olcRefintAttribute: uniqueMember
olcRefintAttribute: krbObjectReferences
olcRefintAttribute: krbPwdPolicyReference
olcRefintNothing: cn=admin,$LDAP_SUFFIX
EOF
echo " = unique = "
$LDAPADD << EOF
dn: olcOverlay=unique,olcDatabase={1}hdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcUniqueConfig
olcOverlay: unique
olcUniqueURI: ldap:///ou=gente,$LDAP_SUFFIX?mail,employeeNumber?sub?(objectClass=inetOrgPerson)
olcUniqueURI: ldap:///ou=grupos,$LDAP_SUFFIX?gidNumber?one?(objectClass=posixGroup)
olcUniqueURI: ldap:///ou=gente,$LDAP_SUFFIX?uidNumber?one?(objectClass=posixAccount)
EOF
echo " = constraint = "
$LDAPADD << EOF
dn: olcOverlay=constraint,olcDatabase={1}hdb,cn=config 
changetype: add
objectClass: olcOverlayConfig
objectClass: olcConstraintConfig
olcOverlay: constraint
olcConstraintAttribute: jpegPhoto size 131072
olcConstraintAttribute: userPassword count 5
olcConstraintAttribute: uidNumber regex ^[[:digit:]]+$
olcConstraintAttribute: gidNumber regex ^[[:digit:]]+$
EOF
echo " = password policy = "
# cargamos el overlay
$LDAPADD << EOF
dn: olcOverlay=ppolicy,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
olcPPolicyDefault: cn=default,cn=politicas,cn=servicios,$LDAP_SUFFIX
olcPPolicyHashCleartext: TRUE
olcPPolicyUseLockout: FALSE
EOF
# cargamos las reglas del password policy
$LDAPADDUSER -D "cn=admin,$LDAP_SUFFIX" -w $PASS << EOF
dn: cn=default,cn=politicas,cn=servicios,$LDAP_SUFFIX
cn: default
objectClass: pwdPolicy
objectClass: person
objectClass: top
pwdAllowUserChange: TRUE
pwdAttribute: userPassword
pwdCheckQuality: 2
pwdExpireWarning: 600
pwdFailureCountInterval: 30
pwdGraceAuthNLimit: 5
pwdInHistory: 5
pwdLockout: TRUE
pwdLockoutDuration: 0
pwdMaxAge: 0
pwdMaxFailure: 5
pwdMinAge: 0
pwdMinLength: 5
pwdMustChange: FALSE
pwdSafeModify: FALSE
sn: dummy value
EOF
echo " = MemberOf = "
$LDAPADD << EOF
dn: olcOverlay=memberof,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: olcConfig
objectClass: top
olcOverlay: memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
EOF
echo " = Dynamic Listing = "
$LDAPADD << EOF
dn: olcOverlay=dynlist,olcDatabase={1}hdb,cn=config 
changetype: add
objectClass: olcOverlayConfig
objectClass: olcDynamicList
olcOverlay: dynlist
olcDLattrSet: {0}groupOfURLs memberURL member
olcDLattrSet: {1}labeledURIObject labeledURI memberUid:uid
olcDLattrSet: {2}groupOfNames labeledURI member
olcDLattrSet: {3}groupOfURLs memberURL memberUid:uid
EOF
echo " = Val Sort = "
$LDAPADD << EOF
dn: olcOverlay=valsort,olcDatabase={1}hdb,cn=config 
changetype: add
objectClass: olcOverlayConfig
objectClass: olcValSortConfig
olcOverlay: valsort
olcValSortAttr: memberUid ou=grupos,$LDAP_SUFFIX alpha-ascend
olcValSortAttr: uid ou=gente,$LDAP_SUFFIX alpha-ascend
olcValSortAttr: displayName ou=gente,$LDAP_SUFFIX alpha-ascend
olcValSortAttr: uidNumber ou=gente,$LDAP_SUFFIX num-ascend
EOF
echo " = SyncProv = "
# configuracion de overlay de sincronia para hdb(1)
$LDAPADD << EOF
dn: olcOverlay=syncprov,olcDatabase={1}hdb,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcSyncProvConfig
olcOverlay: syncprov
olcSpCheckpoint: 20 10
olcSpSessionlog: 500
olcSpNoPresent: TRUE
EOF
# sincronia de la db-config
$LDAPADD << EOF
dn: olcOverlay=syncprov,olcDatabase={0}config,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcSyncProvConfig
olcOverlay: syncprov
olcSpCheckpoint: 20 10
olcSpSessionlog: 500
olcSpNoPresent: TRUE
EOF
}