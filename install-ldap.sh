#!/bin/bash -e
#
# ==============================================================================
# GNU-LDAP: 
# COPYRIGHT:
#  (C) 2012 Jesús Lara Giménez <jesuslara@devel.com.ve>>
# LICENCIA: GPL3
# ==============================================================================
#
# Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los
# términos de la Licencia Pública General de GNU (versión 3).

# gnu-ldap ###
# script para instalar y configurar un directorio basado en openLDAP
#############################################################
## Root needed

if [ "`id -u`" != "0" ]; then
$CAT << _MSG
  ---------------------------------------
  | Error !!                            |
  | Es necesario ser root para instalar |
  | Y configurar openLDAP               |
  |                                     |
  | No se instalará nada!               |
  ---------------------------------------
_MSG
	exit 1
fi

####===============================================####

COMMAND=$1
DIRECTORY="$(pwd)"

# e incluyo mi propia configuracion
if [ -f $DIRECTORY/install-ldap.conf ] ; then
   . $DIRECTORY/install-ldap.conf
else
   echo "No se ha encontrado el archivo de configuracion";
   exit 1
fi

# functions script
if [ -f "$DIRECTORY/scripts/functions.sh" ] ; then
   . $DIRECTORY/scripts/functions.sh
else
   echo "functions script not found, stop";
   exit 1
fi

# == main execution ==

usage() {
    echo "Usage: $(readlink -f $0) {install|uninstall|backup|test}"
    exit 0
}

help() {
    cat <<EOF

This script is a helper to install and configure openLDAP in
Debian systems.

The script will install openldap, configure DIT for ldap tree
overlays, logging and ACL.

REMEMBER: configure script in install-ldap.conf

install-ldap install"

EOF
}

# Main program

# si no pasamos ningun parametro
if [ $# = 0 ]; then
    usage
fi

## step 1: pre-configure
#
pre_configure() {
# mostrando un mapa basico de instalacion
test

## configurando archivos:

## eliminamos toda referencia erronea del /etc/hosts
$SED -i "/^$LAN_IPADDR*/d" /etc/hosts

# modificar /etc/hosts para incorporar resolucion de nombre de equipo
echo "# direccion ip del host" >> /etc/hosts
echo "$LAN_IPADDR  $SERVERNAME $HOSTNAME_PREFIX" >> /etc/hosts

# asignar nombre al servidor:
echo "$SERVERNAME" > /etc/hostname
$HOSTNAME $SERVERNAME

# y del dominio
$DOMAINNAME $DOMAIN

# y reiniciamos el servicio
/etc/init.d/hostname.sh start

# configuro el host.conf para resolucion mixta
cat <<EOF > /etc/host.conf
multi on
order hosts,bind

EOF

# incorporo las reglas del LDAP en host.allow
echo "slapd: $LAN_IPADDR" >> /etc/hosts.allow

# configure ldap.conf
cat <<EOF > /etc/ldap/ldap.conf
#
# LDAP Defaults
#
BASE    $LDAP_SUFFIX
URI     ldap://$SERVERNAME:389 ldaps://$SERVERNAME:636
SIZELIMIT       200
TIMELIMIT       30
EOF

echo
echo "Finalizando pre-configuracion, instalando ..."
echo

}

## step 3: install LDAP
#
install() {
# pre-configuramos la instalacion
pre_configure

# instalar utilitarios
setup_utilities

# instalando LDAP server
ldap_setup

sleep 1

# ldap script
if [ -f "$DIRECTORY/scripts/ldap_functions.sh" ] ; then
   . $DIRECTORY/scripts/ldap_functions.sh
else
   echo "LDAP functions script not found, stop";
   exit 1
fi

# configurando servidor ldap
ldap_configure

# configurando logging
log_configure

sleep 1

# configuracion de schemas
schema_configure

# carga del DIT basico
dit_configure

# ACL configure
acl_configure

# configuramos SASL
sasl_configure

# seguridad SSL
ssl_configure

# carga de modulos
auditlog_configure
accesslog_configure
cnmonitor_configure

sleep 1
overlay_configure

echo
echo "Finalizando, reiniciando el servicio..."
$LDAP_SERVICE restart
sleep 1

echo
echo " [ OpenLDAP Instalado ]"
echo
echo "Recuerde: el administrador bind dn es:"
echo "cn=admin,$LDAP_SUFFIX"
}

test() {
get_distro

# deteccion de interface
firstdev
LAN_IPADDR="$($IP addr show $LAN_INTERFACE | awk "/^.*inet.*$LAN_INTERFACE\$/{print \$2}" | sed -n '1 s,/.*,,p')"

# nombre del servidor
servername

# dominio y prefijo
get_suffix

	$CAT << _MSG
 ************************** [ Instalando OpenLDAP ] *************************
 *
 * Distribucion : .................. $DIST
 * Dominio : ....................... $DOMAIN
 * LDAP Hostname : ................. $SERVERNAME
 * Sufijo LDAP : ................... $LDAP_SUFFIX
 * Interfaz Privada : .............. $LAN_INTERFACE
 * IP Privada : .................... $LAN_IPADDR
 *
 ****************************************************************************
_MSG
}

uninstall() {
# info
get_distro
# desinstalar
$INSTALLER remove --purge $PACKAGES $LDAP_SERVER $SASL_PKGS
$INSTALLER purge $LDAP_SERVER
# remocion de directorios
rm -fR /var/lib/ldap
# reparar configuracion a estado previo?
}

### main execution program

case "$COMMAND" in
    -h|-help|--help) help;;
    install)   install;;
    uninstall)  uninstall;;
    test)    test;;
    backup)  backup;;
    *)        usage;;
esac

exit 0;